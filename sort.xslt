<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
    <output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <strip-space elements="*"/>

    <template match="@*|node()">
        <copy>
            <apply-templates select="@*">
                <sort select="name()"/>
            </apply-templates>
            <apply-templates select="node()">
                <sort select="name()"/>
            </apply-templates>
        </copy>
    </template>
</stylesheet>
